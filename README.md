Ansible Hadoop Cluster
======================

Playbooks
---------
* deploy-gateway.yml: configure the VM that will act as gateway with iptables and dnsmasq. 
* deploy-cluster.yml: deploys and configure a Hadoop cluster.
* deploy-vms.yml: creates VMs in ULL-IaaS infrastructure with cloud-init.
* destroy-vms.yml: destroys VMs in ULL-IaaS infrastructure.
* run-examples.yml: run a Hadoop example to test the cluster.
* start-hadoop.yml: start required services for Hadoop Cluster.
* start-ssh-tunnels.yml: start SSH tunnel to enable port forwarding to Hadoop UI.
* stop-hadoop.yml: stop required services for Hadoop Cluster.
* stop-ssh-tunnels.yml: stop SSH tunnel to disable port forwarding to Hadoop UI.

Examples
--------
`$ ansible-playbook --ask-vault-pass deploy-vms.yml`

`$ ansible-playbook -i inventory/production deploy-hadoop.yml`

License
-------
GNU GPLv3

Authors
-------
