---
# tasks/user.yml

- name: Add user for Hadoop
  ansible.builtin.user:
    comment: Hadoop User
    name: hadoop
    groups: sudo
    append: yes
    password: "{{ lookup('password', '/dev/null length=8') | password_hash('sha256') }}"
    update_password: "on_create"
    generate_ssh_key: true
    shell: /bin/bash
    state: present

- name: Copy predefined SSH keys to namenode
  ansible.builtin.copy:
    src: "{{ item.src }}"
    dest: "/home/hadoop/.ssh/{{ item.dest }}"
    owner: hadoop
    group: hadoop
    mode: "{{ item.mode }}"
  with_items:
    - { src: 'hadoop-id_rsa', dest: 'id_rsa', mode: '0400' }
    - { src: 'hadoop-id_rsa.pub', dest: 'id_rsa.pub', mode: '0644' }
  when: inventory_hostname in groups['hadoop_namenode']

- name: Add Hadoop user SSH public key to authorized_key
  ansible.posix.authorized_key:
    user: hadoop
    state: present
    key: "{{ lookup('file', 'hadoop-id_rsa.pub') }}"
    exclusive: true

- name: Ensure that Hadoop user does not need password
  ansible.builtin.lineinfile:
    path: /etc/sudoers.d/hadoop
    regexp: "^%hadoop ALL="
    line: "%hadoop ALL=(ALL)  NOPASSWD: ALL"
    state: present
    create: yes
    backup: yes
    validate: visudo -cf %s

- name: Set user specific environment variables
  ansible.builtin.blockinfile:
    path: "/home/hadoop/{{ item }}"
    create: true
    state: present
    block: |
      PATH="$HADOOP_HOME/bin:$HADOOP_HOME/sbin:$PATH"
  with_items:
    - .bashrc
    - .profile

- name: Set system wide environment variables
  ansible.builtin.blockinfile:
    path: /etc/environment
    create: true
    state: present
    block: |
      JAVA_HOME="/usr/lib/jvm/java-11-openjdk-amd64"
      HADOOP_HOME="{{ hadoop_binary_dir }}"
