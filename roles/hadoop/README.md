hadoop
============
This role configures a Hadoop Cluster.

Variables
---------
| Variable                 | Description                              | Example                                                                                                   |
|--------------------------|------------------------------------------|-----------------------------------------------------------------------------------------------------------|
| hadoop_version           | Version to deploy.                       | 3.3.6                                                                                                     |
| hadoop_download_url      | URL to download the Hadoop binary.       | https://downloads.apache.org/hadoop/common/hadoop-{{ hadoop_version }}/hadoop-{{ hadoop_version }}.tar.gz |
| hadoop_download_path     | Temp path to download the Hadoop binary. | /tmp/hadoop-{{ hadoop_version }}.tar.gz                                                                   |
| hadoop_download_checksum | Checksum to secure download.             | sha512:..............                                                                                     |
| hadoop_binary_dir        | Path to the directory to install Hadoop. | /home/hadoop/hadoop                                                                                       |
| hadoop_dfs_replicas      | Indicates the DFS replicas to configure. | 2                                                                                                         |
| hadoop_namenode_uri      | URI of the Hadoop cluster NameNode.      | namenode.example.org                                                                                      |
| hadoop_namenode_port     | Listening port of NameNode.              | 9000                                                                                                      |
| hadoop_workers           | A list with the workers URIs.            | ["worker1.example.org", "worker2.example.org"]                                                            | 

License
-------
GNU GPLv3

Author Information
------------------
